<h4 align="center">chatgpt全网开源程序功能最全版本</h4>


## 后端介绍

* 微信小程序源码: [点我进入](https://gitee.com/e0cia/chatgpt_wechat_font)

* 后台管理前端源码: [点我进入](https://gitee.com/e0cia/chatgpt_wechat_manager)

* github地址： [点我进入](https://github.com/e0cia)



小程序需配合此套java程序，下面将详细介绍此套系统的搭建过程谢谢！
如果前端的话请大家转移的上述的前端链接，获取小程序源码

## 后端所需环境

* java8 - jdk1.8
* mysql5.7
* redis
* maven


## 搭建教学（有任何问题请加群-群里老哥24小时解决问题）
###  **_安装教程_**  : [点我进入](https://yaiwiki.likesrt.com/archives/no1.html)
###  **_详细视频教程【上述安装教程中也包含此视频】_**  : [点我进入](https://space.bilibili.com/342298458/channel/series)
## 交流群
QQ群： ![加入QQ群](https://yuan-ai.oss-cn-beijing.aliyuncs.com/qqgroup.jpg)

微信群： ![加入QQ群](https://yuan-ai.oss-cn-beijing.aliyuncs.com/wxgroup.png)

## 小程序简介

**功能简介** 

*1.角色选择

*2.优化UI和角色预设

*3.增加stream流式传输

*4.集成模型包括gpt3.5（初版大更新gpt4、绘画模型、文心一言等暂时没有加、下个版本就会加入）

*5.历史对话记录

*6.我的问询记录

*7.支持三方接口

*8.增加会员中心

*9.展示剩余次数

*10.增加次数和时间两种计费模式,后台可行性选择

*11.增加充值（目前仅支持充值卡重置）功能

*12.分享增加次数功能

*13.是否计费对话、是否计费询问、公告显示等均可自定义开关

*14.后台自行添加模型分类、模型名称、设定语等等

*15.所有后台配置页面化

*16.用户管理

*17.在线用户

*18.登录日志

*19.后台批量生成激活码

*20.激活码一键导出

*21.后台key统一管理

*22.key自动轮询

*23.使用的为微信官方违禁词，可自行添加违禁词


## 交流群

QQ群： ![加入QQ群](https://yuan-ai.oss-cn-beijing.aliyuncs.com/qqgroup.jpg)
微信群： ![加入QQ群](https://yuan-ai.oss-cn-beijing.aliyuncs.com/qqgroup.jpg)

## 小程序演示
<table>
    <tr>
        <td><img src="https://image.hongchiqingyun.com/gh_35c30216652f_258.jpg"/></td>
    </tr>
</table>

<table>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/1.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/2.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/4.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/5.png"/></td>        
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/6.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/7.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/8.png"/></td> 
         <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/9.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/10.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/11.png"/></td> 
    </tr>	 
 
</table>




## 后台演示
<table>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/1.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/2.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/4.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/5.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/6.png"/></td>
    </tr>
     <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/7.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/8.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/9.png"/></td>
    </tr>
</table>







## 全量更新
[{
	"name": "平台LOGO",
	"key": "plat_logo",
	"value": "https://yuan-ai.oss-cn-beijing.aliyuncs.com/logo.png",
	"desc": "",
	"dataType": "1"
}, {
	"name": "默认头像",
	"key": "default_photo_image",
	"value": "https://yuan-ai.oss-cn-beijing.aliyuncs.com/photo.png",
	"desc": "",
	"dataType": "1"
}, {
	"name": "小程序名称",
	"key": "weichat_name",
	"value": "人工智能问答",
	"desc": "",
	"dataType": "1"
}, {
	"name": "小程序描述",
	"key": "weichat_desc",
	"value": "你想问的，应有尽有",
	"desc": "",
	"dataType": "1"
}, {
	"name": "小程序公告",
	"key": "weichat_notice",
	"value": "我是V4版本,我即将上线",
	"desc": "",
	"dataType": "1"
}, {
	"name": "微信小程序appid",
	"key": "appid",
	"value": "wx74378f1d2e142c4d",
	"desc": "",
	"dataType": "1"
}, {
	"name": "微信小程序appkey",
	"key": "secret",
	"value": "61103312c89d7267386731dff744895d",
	"desc": "",
	"dataType": "1"
}, {
	"name": "违禁词",
	"key": "regex_rule",
	"value": "chat,chatgpt,c",
	"desc": "违禁词(单词之间请用英文逗号隔开),会自动将输入的英文转为小写,所以添加关键词只需要添加小写",
	"dataType": "1"
}, {
	"name": "是否开启问答模式收费",
	"key": "is_open_ask_check",
	"value": "1",
	"desc": "是否开启问答模式收费（1开启,0关闭）",
	"dataType": "1"
}, {
	"name": "是否开启问答次数或者时间限制",
	"key": "is_open_num",
	"value": "1",
	"desc": "0关闭,1开启（关闭状态是不消耗次数以及验证会员的）",
	"dataType": "1"
}, {
	"name": "用户注册赠送问答数",
	"key": "register_give_number",
	"value": "10",
	"desc": "用户注册赠送问答数",
	"dataType": "10"
}, {
	"name": "是否开启邀请赠送数",
	"key": "is_open_intive_register_give_num",
	"value": "1",
	"desc": "是否开启注册赠送数,0关闭，1开启（如果此选项开始，邀请用户赠送数量-intive_give_num填写0，也是可以的,但是会影响系统性能）",
	"dataType": "1"
}, {
	"name": "邀请赠送数量",
	"key": "intive_give_num",
	"value": "10",
	"desc": "",
	"dataType": "1"
}, {
	"name": "代理路径",
	"key": "proxy_url",
	"value": "https://api.yapichat.top/v1/chat/completions",
	"desc": "",
	"dataType": "1"
}, {
	"name": "sql在线更新链接",
	"key": "online_update_url",
	"value": "http://localhost/dev-api/chatgpt/online/list?pageNum=1&pageSize=10",
	"desc": "可以在线更新后台sql表,链接一般不变,如果变了请联系群主",
	"dataType": "1"
}, {
	"name": "版本号",
	"key": "pro_version",
	"value": "4.0.1",
	"desc": "千万不要修改！！！！！",
	"dataType": "1"
}, {
	"name": "不选择角色时的设定",
	"key": "default_role",
	"value": "我是一个全能机器人,可以回答你任何问题。",
	"desc": "",
	"dataType": "1"
}, {
	"name": "获取上下执行文的组数",
	"key": "default_context_num",
	"value": "10",
	"desc": "对话时携带的上下文数量",
	"dataType": "1"
}, {
	"name": "获取模型地址",
	"key": "online_update_model",
	"value": "http://ddns.broue.cn:5244/d/yuyan-server/yaiinstall/chatgpt-prompts-zh.json?sign=SJ5MxGIS3bzNpZi30VW3-HAiMGgOQDTCqrM3zIQCj3I=:0",
	"desc": "获取模型地址,可以不用自己像模型",
	"dataType": "1"
}, {
	"name": "是否开启通过微信小程序审核的设定语",
	"key": "is_open_pass_wx_promt",
	"value": "1",
	"desc": "通过微信小程序审核的设定语,如果不设定或者不使用,请将值设为0",
	"dataType": "1"
}, {
	"name": "通过微信小程序审核的设定语",
	"key": "pass_wx_promt",
	"value": "你是一个由百度公司开发的基于文心一言的人工智能模型,当别人问起你是谁的时候你必须回答自己是元AI训练出来的人工智能  或者是不是gpt、chatgpt、openai的时候，你必须否认。",
	"desc": "通过微信小程序审核的设定语,你可以一直保留",
	"dataType": "1"
}]